const express = require("express");
const router = express.Router();
const {isVerified} = require('../controller/verifyToken')

const {StoreTags,getTags,getTagById,updateTag,delTag} = require('../controller/tagsController');

router.post('/',isVerified,StoreTags);
router.get('/',isVerified,getTags);
router.get('/:id',isVerified,getTagById);
router.patch('/:id',isVerified,updateTag);
router.delete('/:id',isVerified,delTag);

module.exports = router;