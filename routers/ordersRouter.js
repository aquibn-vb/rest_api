const express = require('express');
const router = express.Router();
const {isVerified} = require('../controller/verifyToken')
const {storeOrders , getOrders , getOrdersByUserId , updateOrderDetails , delOrder} = require('../controller/ordersController');

router.post('/',isVerified,storeOrders);
router.get('/',isVerified,getOrders);
router.get('/userOrders',isVerified,getOrdersByUserId );
router.patch('/:id',isVerified,updateOrderDetails);
router.delete('/:id',isVerified,delOrder);

module.exports = router;
