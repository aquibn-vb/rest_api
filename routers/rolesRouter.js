const express = require("express");
const router = express.Router();
const {isVerified} = require('../controller/verifyToken')

const {getRoles,StoreRoles,getRoleByID,updateRole,deleteRole} = require('../controller/rolesController')

router.post('/',isVerified,StoreRoles);

router.get('/',isVerified,getRoles);

router.get('/:id',isVerified,getRoleByID);

router.patch('/:id', isVerified,updateRole);

router.delete('/:id',isVerified,deleteRole);

module.exports = router;