const express = require('express');
const router = express.Router();
const {storeItemInCart,getCarts,getCartsByUserId,updateTotalPrice,getCartById,delCartDoc} = require('../controller/cartsController');
const {isVerified} = require('../controller/verifyToken')

router.post('/',isVerified,storeItemInCart);
router.get('/',isVerified,getCarts);
router.get('/userCart',isVerified,getCartsByUserId);
router.get('/:id',isVerified,getCartById);
router.patch('/:id',isVerified,updateTotalPrice);
router.delete('/:id',isVerified,delCartDoc);

module.exports = router;