const express = require("express");
const router = express.Router();
const {isVerified} = require('../controller/verifyToken')

const {StoreProducts,getProducts,getSpecificProducts,updateProductPrice,delProduct} = require('../controller/productsController');

router.post('/',isVerified,StoreProducts);
router.get('/',isVerified,getProducts);
router.get('/search',isVerified,getSpecificProducts);
router.patch('/:id',isVerified,updateProductPrice);
router.delete('/:id',isVerified,delProduct);

module.exports = router;