const express = require("express");
const router = express.Router();
const {isVerified} = require('../controller/verifyToken')

const {getUsers,StoreUser,getUsersById,updateUser,deleteUser} = require('../controller/userController')

router.get('/',isVerified,getUsers)

router.get('/:id',isVerified,getUsersById)

router.post('/',StoreUser);

router.patch('/:id',isVerified, updateUser);

router.delete('/:id',isVerified, deleteUser)

// router.post('/login',login);

module.exports = router;