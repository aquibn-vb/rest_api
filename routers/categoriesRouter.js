const express = require("express");
const router = express.Router();
const {isVerified} = require('../controller/verifyToken')
const {StoreCategories,getCategories,getCategoryByID,updateCategory,deleteCategory} = require('../controller/categoriesController');

router.post('/',isVerified,StoreCategories);
router.get('/',isVerified,getCategories);
router.get('/:id',isVerified,getCategoryByID);
router.patch('/:id',isVerified,updateCategory);
router.delete('/:id',isVerified,deleteCategory);

module.exports = router;