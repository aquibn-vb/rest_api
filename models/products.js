const mongoose = require('mongoose');

const productsSchema = new mongoose.Schema({
    name:{
        type:String
        // required:true
    },
    thumbnail :{
        type:String,
        required:true
    },
    product_gallary:{
        type:Array,
        required:true
    },
    description :{
        type:String
    },
    base_price : {
        type: Number,
        required:true
    },
    sell_price : {
        type: Number,
        required:true
    },
    category_name : {
        type:String
    },
    tags : {
        type : String
    },
    additional_info : {
        type : Object
    }
})

module.exports = mongoose.model('products',productsSchema);