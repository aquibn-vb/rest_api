const mongoose = require('mongoose');

const ordersSchema = new mongoose.Schema({
    user_id:{
        type:mongoose.Schema.Types.ObjectId,
        required:true
    },
    total_items : {
        type:Number
    },
    products : {
        type:Array
    },
    billing_address : {
        type:Object
    },
    shipping_address : {
        type:Object
    },
    transaction_status : {
        type:String
    },
    payment_mode : {
        type:String
    },
    payment_status : {
        type : String
    },
    order_status : {
        type:String
    }
})

module.exports = mongoose.model('orders',ordersSchema)