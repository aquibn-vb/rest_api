const mongoose = require('mongoose')

const cartsSchema = new mongoose.Schema({
    product:{
        type:mongoose.Schema.Types.ObjectId,
        required:true
    },
    user:{
        type:mongoose.Schema.Types.ObjectId,
        required:true
    },
    product_qty : {
        type : Number
    },
    base_price : {
        type : Number
    },
    sell_price : {
        type : Number
    },
    total_price : {
        type : Number

    },
})

module.exports = mongoose.model('carts',cartsSchema);