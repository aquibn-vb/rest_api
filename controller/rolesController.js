const roles = require('../models/roles')

const getRoles = async (req,res) => {
    try {
        const all_roles = await roles.find();
        res.json(all_roles)
    } catch (error) {
        res.send("error"+error);
    }
}

const StoreRoles = async (req,res) => {
    const role = new roles({
        name: req.body.name,
        slug: req.body.slug,
    })
    try {
       const data = await role.save();
       res.json(data);
    } catch (error) {
        console.log("error")
    }
}

const getRoleByID = async (req,res) => {
    try {
        const particular_role = await roles.findById(req.params.id);
        res.json(particular_role)
    } catch (error) {
        res.send("error"+error);
    }
}

const updateRole = async (req,res) => {
    try {
        const role = await roles.findById(req.params.id);
        role.name = req.body.name;
        role.slug = req.body.slug;
        const updated = await role.save();
        res.json(updated);
    } catch (error) {
        console.log("error")
    }
}

const deleteRole = async (req,res) => {
    try {
        const role = await roles.findById(req.params.id);
        const deleted = await role.remove();
        res.json(deleted);
    } catch (error) {
        console.log("error")
    }
}

module.exports = {getRoles,StoreRoles,getRoleByID,updateRole,deleteRole}