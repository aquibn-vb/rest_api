const orders = require('../models/orders')

const storeOrders = async (req,res) => {
    const OrderToStore = new orders({
        user_id : req.body.user_id,
        total_items : req.body.total_items,
        products : req.body.products,
        billing_address : req.body.billing_address,
        shipping_address : req.body.shipping_address,
        transaction_status : req.body.transaction_status,
        payment_mode : req.body.payment_mode,
        payment_status : req.body.payment_status,
        order_status : req.body.order_status
    })
    try {
        const storedOrder = await OrderToStore.save();
        res.send(storedOrder)
    } catch (error) {
        res.send("error"+error)
    }
}

const getOrders = async (req,res) => {
    try {
        const all_orders = await orders.find();
        res.json(all_orders);
    } catch (error) {
        res.send("error"+error)
    }
}

const getOrdersByUserId = async (req,res) => {
    try {
        const filters = req.query;
        const all_orders = await orders.find();
        const filteredUserOrder = all_orders.filter(order => {
            let isValid = true;
            for (key in filters){
                isValid = isValid && order[key] == filters[key];
            }
            return isValid;
        })
        res.json(filteredUserOrder);
    } catch (error) {
        res.send("error"+error)
    }
}

const updateOrderDetails = async (req,res) => {
    try {
        const orderToUpdate = await orders.findById(req.params.id);
        orderToUpdate.user_id = req.body.user_id,
        orderToUpdate.total_items = req.body.total_items,
        orderToUpdate.products = req.body.products,
        orderToUpdate.billing_address = req.body.billing_address,
        orderToUpdate.shipping_address = req.body.shipping_address,
        orderToUpdate.transaction_status = req.body.transaction_status,
        orderToUpdate.payment_mode = req.body.payment_mode,
        orderToUpdate.payment_status = req.body.payment_status,
        orderToUpdate.order_status = req.body.order_status

        const updated = await orderToUpdate.save();
        res.json(updated);
    } catch (error) {
        res.send("error=>"+error)
    }
}

const delOrder = async (req,res) => {
    try {
        const orderToDel = await orders.findById(req.params.id);
        const deleted = await orderToDel.remove();
        res.send(deleted)
    } catch (error) {
        res.send("error => "+error)       
    }
}

module.exports = {storeOrders , getOrders , getOrdersByUserId , updateOrderDetails , delOrder}