const tags = require('../models/tags')

const StoreTags = async (req,res) => {
    const tag = new tags({
        name: req.body.name,
        slug: req.body.slug,
    })
    try {
       const data = await tag.save();
       res.json(data);
    } catch (error) {
        console.log("error"+error);
    }
}

const getTags = async (req,res) => {
    try {
        const all_tags = await tags.find();
        res.json(all_tags)
    } catch (error) {
        res.send("error"+error);
    }
}
const getTagById = async (req,res) => {
    try {
        const tag = await tags.findById(req.params.id);
        res.json(tag);
        
    } catch (error) {
        res.send("error"+error)
    }
}
const updateTag = async (req,res) => {
    try {
        const tagToUpdate = await tags.findById(req.params.id);
        tagToUpdate.name = req.body.name;
        tagToUpdate.slug = req.body.slug;
        const updated = await tagToUpdate.save();
        res.json(updated);
    } catch (error) {
        res.send("error"+error)
    }
}
const delTag = async (req,res) => {
    try {
        const tagToDel = await tags.findById(req.params.id);
        const deleted = await tagToDel.remove();
        res.send(deleted)
    } catch (error) {
        res.send("error"+error)
    }
}

module.exports = {StoreTags,getTags,getTagById,updateTag,delTag}