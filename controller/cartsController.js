const carts = require('../models/carts')

const storeItemInCart = async (req,res) => {
    const itemToStoreInCart = new carts({
        product : req.body.product,
        user : req.body.user,
        product_qty : req.body.product_qty,
        base_price : req.body.base_price,
        sell_price : req.body.sell_price,
        total_price : req.body.total_price
    })
    try {
        const storedItem = await itemToStoreInCart.save();
        res.send(storedItem);
    } catch (error) {
        res.send("error"+error)
    }
}
const getCarts = async (req,res) => {
    try {
        const allDocsInCart = await carts.find();
        res.json(allDocsInCart);
    } catch (error) {
        res.send("error"+error)
    }
}

const getCartsByUserId = async (req,res) => {
    try {
        const filters = req.query;
        const allDocsInCart = await carts.find();
        const filteredCarts = allDocsInCart.filter(cart => {
            let isValid = true;
            for (key in filters){
                isValid = isValid && cart[key] == filters[key]
            }
            return isValid
        });
        res.json(filteredCarts); 
    } catch (error) {
        res.send("error"+error);
    }
}
const getCartById = async (req,res) => {
    try {
        const cartDoc = await carts.findById(req.params.id);
        res.json(cartDoc) 
        
    } catch (error) {
        res.send("error "+ error)
    }
}

const updateTotalPrice = async (req,res) => {
    try {
        console.log(req.user);
        const itemToUpdate = await carts.findById(req.params.id);
        itemToUpdate.product_qty = req.body.product_qty;
        itemToUpdate.total_price =  itemToUpdate.sell_price * itemToUpdate.product_qty;
        const updated = await itemToUpdate.save();
        res.json(updated)
    } catch (error) {
        res.send("error"+error);
    }
}
const delCartDoc = async (req,res) => {
    try {
        const cartDocToDel = await carts.findById(req.params.id);
        const deleted = await cartDocToDel.remove();
        res.json(deleted);
    } catch (error) {
        res.send("error"+error)
    }
}

module.exports = {storeItemInCart,getCarts,getCartsByUserId,updateTotalPrice,getCartById,delCartDoc}