const users = require('../models/user')
const bcrypt = require('bcryptjs');

const StoreUser = async (req, res) =>{
    // encrypting password
    const salt = await bcrypt.genSalt(10);
    const encryptedPswrd = await bcrypt.hash(req.body.password,salt)

    const user = new users({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password:encryptedPswrd,
        profile_img: req.body.profile_img,
        role: req.body.role
    })
    try {
       const data = await user.save();
       res.json(data);
    } catch (error) {
        console.log("error")
    }
}

const getUsers = async (req,res) => {
    try {
        const all_users = await users.find();
        res.json(all_users)
    } catch (error) {
        res.send("error"+error);
    }
}

const getUsersById = async (req,res) => {
    try {
        const particular_user = await users.findById(req.params.id);
        res.json(particular_user)
    } catch (error) {
        res.send("error"+error);
    }
}
const updateUser = async (req,res) => {
    try {
        const user = await users.findById(req.params.id);
        user.role = req.body.role;
        const updated = await user.save();
        res.json(updated);
    } catch (error) {
        console.log("error")
    }
}
const deleteUser = async (req,res) => {
    try {
        const user = await users.findById(req.params.id);
        const deleted = await user.remove();
        res.json(deleted);
    } catch (error) {
        console.log("error")
    }
}



module.exports = {StoreUser , getUsers , getUsersById , updateUser , deleteUser }