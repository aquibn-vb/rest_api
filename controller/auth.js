const users = require('../models/user')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');


const login = async (req,res) => {
    // checking if the user exists through emailID
    const user = await users.findOne({email:req.body.email});
    if(!user) return res.status(400).send("email or password is wrong");

    // checking if the password of found user is correct or not
    const validPswrd = await bcrypt.compare(req.body.password,user.password);
    if(!validPswrd) return res.status(400).send("password incorrect");

    // creating and assigning a token
    const token = jwt.sign({_id:user._id},process.env.TOKEN_SECRET);
    res.header('auth-token',token).send(token);
}

module.exports = {login}

