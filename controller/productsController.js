const products = require('../models/products')

const StoreProducts = async (req,res) => {
    const productToStore = new products({
        name : req.body.name,
        thumbnail : req.body.thumbnail,
        product_gallary : req.body.product_gallary,
        description : req.body.description,
        base_price : req.body.base_price,
        sell_price : req.body.sell_price,
        category_name : req.body.category_name,
        tags : req.body.tags,
        additional_info : req.body.additional_info
    })
    try {
        const savedProduct = await productToStore.save();
        res.send(savedProduct)
    } catch (error) {
        res.send("error"+error)
    }
}

const getProducts = async (req,res) => {
    try {
        const all_products = await products.find();
        res.json(all_products);
    } catch (error) {
        res.send("error"+error)
    }
}

const getSpecificProducts = async (req,res) => {
    try{
        const filters = req.query; //req.query return an object of key value pairs. These key values come from params.
        const all_products = await products.find();//all_products will conatain an array of all the documents(objects) in products collection.
        // filtering based on key value pair recieved from params.
        const filteredProducts = all_products.filter(product => {
            let isValid = true;
            for (key in filters) {
              console.log(key, product[key], filters[key]);
              isValid = isValid && product[key] == filters[key];
            }
            return isValid;
          });
          res.json(filteredProducts);
    }
    catch (error) {
        res.send("error"+error)
    }
}

const updateProductPrice = async (req,res) => {
    try {
        const productToUpdate = await products.findById(req.params.id);
        productToUpdate.sell_price = req.body.sell_price;
        const updated = await productToUpdate.save();
        res.json(updated);
    } catch (error) {
        res.send("error"+error)
    }
}
const delProduct = async (req,res) => {
    try {
        const productTodelete = await products.findById(req.params.id);
        const deleted = await productTodelete.remove();
        res.json(deleted);
    } catch (error) {
        res.send("error"+error)
    }
}

module.exports = {StoreProducts,getProducts,getSpecificProducts,updateProductPrice,delProduct}