const categories = require('../models/categories')

const StoreCategories = async (req,res) => {
    const category = new categories({
        name: req.body.name,
        slug: req.body.slug,
        image : req.body.image,
        description : req.body.description
    })
    try {
       const data = await category.save();
       res.json(data);
    } catch (error) {
        console.log("error")
    }
}
const getCategories = async (req,res) => {
    try {
        const all_categories = await categories.find();
        res.json(all_categories)
    } catch (error) {
        res.send("error"+error);
    }
}

const getCategoryByID = async (req,res) => {
    try {
        const particular_category = await categories.findById(req.params.id);
        res.json(particular_category)
    } catch (error) {
        res.send("error"+error);
    }
}
const updateCategory = async (req,res) => {
    try {
        const category = await categories.findById(req.params.id);
        category.name = req.body.name;
        category.slug = req.body.slug;
        category.image = req.body.image,
        category.description = req.body.description
        const updated = await category.save();
        res.json(updated);
    } catch (error) {
        console.log("error")
    }
}
const deleteCategory = async (req,res) => {
    try {
        const category = await categories.findById(req.params.id);
        const deleted = await category.remove();
        res.json(deleted);
    } catch (error) {
        console.log("error")
    }
}

module.exports = {StoreCategories,getCategories,getCategoryByID,updateCategory,deleteCategory}