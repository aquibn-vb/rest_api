const express = require("express");
const mongoose = require("mongoose");
const dotenv = require('dotenv')

dotenv.config();

const url = "mongodb://localhost:27017/ecommerce_DB4"
const port = process.env.PORT || 8000;

const app = express();
// connecting to database
mongoose.connect(url,{useNewUrlParser:true});
const db = mongoose.connection;
// importing routers
const userRouter = require('./routers/users');
const rolesRouter = require('./routers/rolesRouter');
const loginRouter = require('./routers/loginRouter');
const categoriesRouter = require('./routers/categoriesRouter');
const tagsRouter = require('./routers/tagsRouter');
const productsRouter = require('./routers/productsRouter');
const cartsRouter = require('./routers/cartsRouter');
const ordersRouter = require('./routers/ordersRouter')

db.on('open',function(){
    console.log("connected to db..");
})
app.use(express.json());


app.use('/users',userRouter);
app.use('/roles',rolesRouter);
app.use('/login',loginRouter);
app.use("/categories",categoriesRouter);
app.use("/tags",tagsRouter);
app.use("/products",productsRouter);
app.use("/cart",cartsRouter);
app.use('/orders',ordersRouter);

app.listen(port,()=>{
    console.log(`connection is setup at ${port}`);
})